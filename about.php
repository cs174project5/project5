<?php
	include(dbconnect.php);
	session_start();
	if (isset($_SESSION['loggedIn'])) {
	if($_SESSION['loggedIn']) {	// check for login session
		$cookie = $_SESSION['user'];
		$favorites = $_SESSION['favorites'];
	}
}
if (isset($_COOKIE['username'])) {
	if ($_COOKIE['username'] != null) {
		$cookie = $_COOKIE['username'];
		$session = explode(";", $cookie);
		$query = "SELECT * FROM registrationdb WHERE username LIKE '$username'"; // get username from db
		$results = mysqli_query($conn, $query);
		if ($results) {
			$user = $results->fetch_assoc();
			if($session[1] === sha1($session[0].$secret_key)) { // check if cookie is valid
				$_SESSION['user'] = $session[0]; // set session to username
				$username = $_SESSION['user'];
				$status = 0; //disable login
				$_SESSION['loggedIn'] = true;
				echo "OK";
				//Collect favorites
				$query = "SELECT * FROM favorites WHERE username LIKE '$username'"; // get username from db
				$results = mysqli_query($conn, $query);
				if ($results) 
				{
					if ($results->num_rows > 0) 
					{
						$favorites = array();
						while ($row = $results->fetch_assoc()) { array_push($favorites, $row['videoID']); }
						$_SESSION['favorites'] = $favorites;
					}
				}
			}
			else {
				echo "fail"; // invalid cookie, remove it
				unset($_COOKIE['username']);
				setcookie('username',null,time()-3600);
			}
		}
	}
}	
?>

<!DOCTYPE HTML>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>About - Team #5</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div id="header">
		<div class="section">
			<div class="logo">
				<a href="index.php">shaolin</a>
			</div>
			<nav>
				<ul>
					<li>
						<a href="submission.php">SUBMIT</a>
					</li>
					<li>
						<a href="#">CATEGORY</a>
							<ul>
							<li><a href="category.php?cat=Tutorial">Tutorials</a></li>
							<li><a href="category.php?cat=Entertainment">Entertainment</a></li>
							<li><a href="category.php?cat=Application">Application</a></li>
							<li><a href="category.php?cat=Weapon">Weapons</a></li>
							<li><a href="category.php?cat=Group demo">Group demos</a></li>
							<li><a href="category.php?cat=Others">Others</a></li>
							</ul>
					</li>
					<li>
						<a href="#">videos</a>
							<ul>
							<li><a href="list.php">List videos</a></li>
							<li><a href="search.php">Search videos</a></li>
							</ul>
					</li>
					<li class="selected">
						<a href="#">Profile</a>
							<ul>
							<li><a href="register.php">Register</a></li>
							<?php
								echo "<li><a href=\"favorites.php\">Favorites</a></li>";
								if (isset($_SESSION['loggedIn'])) {
									echo "<li><a href=\"logout.php\">Logout</a></li>";
								}
								else
									echo "<li><a href=\"login.php\">Login</a></li>";
							?>
							</ul>
					</li>
					<li>
						<a href="about.php">about</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div id="body">
		<div>
			<div class="featured">
				<div class="article">
					<h2>TEAM PROJECT 5</h2>
					<p>
						TEAM PROJECT 5<br>
						Involving PHP and SQL database<br>
						User registration and sessions<br>
						Shaolin videos<br><br/>
						
						<b>Team members:</b><br>
						Allen Enrile<br>
						Erik Macias<br>
						Mojdeh Keykhanzadeh<br>
						Marvin Caragay<br>
					</p>

				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div>
			<div class="connect">
				<a href="http://twitter.com/#!/sjsu" id="twitter">twitter</a>
				<a href="http://www.facebook.com/sanjosestate" id="facebook">facebook</a>
				<a href="http://pinterest.com/sjsu/" id="pinterest">pinterest</a>
			</div>
			<p>
				&copy; copyright 2014 | all rights reserved.
			</p>
		</div>
	</div>
</body>
</html>