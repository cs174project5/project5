<?php
include("dbconnect.php");
session_start();

$loggedin = false;
$error = false;
$status = -1;

//if (session_status() == PHP_SESSION_ACTIVE && isset($_SESSION['loggedIn'])) {
if (isset($_SESSION['loggedIn'])) {
	if($_SESSION['loggedIn']) {	// check for login session
		$cookie = $_SESSION['user'];
		$status = 0; // disable login if so
	}
}
elseif (isset($_COOKIE['username'])) {
	if ($_COOKIE['username'] != null) {
		$cookie = $_COOKIE['username'];
		$session = explode(";", $cookie);
		$query = "SELECT * FROM registrationdb WHERE username LIKE '$username'"; // get username from db
		$results = mysqli_query($conn, $query);
		if ($results) {
			$user = $results->fetch_assoc();
			if($session[1] === sha1($session[0].$secret_key)) { // check if cookie is valid
				$_SESSION['username'] = $session[0]; // set session to username
				$status = 0; //disable login
				$_SESSION['loggedIn'] = true;
				echo "OK";
			}
			else {
				echo "fail"; // invalid cookie, remove it
				unset($_COOKIE['username']);
				setcookie('username',null,time()-3600);
			}
		}
	}
}


// Check if the form has been submitted:
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$username = htmlspecialchars($_POST['username']);
	$password = htmlspecialchars($_POST['password']);

	date_default_timezone_set('US/Pacific');
	$currentTime = time();

	// prepare statement
	if ( !($stmt = $conn->prepare("SELECT Username, Password FROM registrationdb WHERE username LIKE (?)")) ) {
		$error = true;
    	echo "<span style=\"color:red\">Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error . "</span>";    	
	}

	// bind parameters
	if ( !$stmt->bind_param("s", $username) ) {
		$error = true;
    	echo "<span style=\"color:red\">Prepare failed: (" . $stmt->errno . ") " . $stmt->error . "</span>";
	}

	// execute statement
	if ( !$stmt->execute() ) {
		$error = true;
    	echo "<span style=\"color:red\">Prepare failed: (" . $stmt->errno . ") " . $stmt->error . "</span>";
	}

	//If no errors, check login
	if (!$error)
	{
		//$username = $_POST['username'];
		//$password = $_POST['password'];

		//check for results
		//$results = $stmt->get_result(); //does not work for php 5.2
		$stmt->bind_result($dbusername, $dbpassword);
		
		$stmt->fetch();

		if ($dbusername)
		{
			//$row = $dbusername->fetch_assoc();	//Get results
			//$user = $row['username'];
			//$userpassw = $row['password'];

			$pw_check = sha1($password.$secret_key);

			if ($pw_check === $dbpassword) //Password match check
			{
				$loggedin = true;
				$status = 0; // login success
				$_SESSION['user'] = $username;
				$_SESSION['loggedIn'] = true;
				$_SESSION['timeLoggedIn'] = $currentTime;
				$_SESSION['favorites'] = array();

				// free result
				$stmt->close();
				
				if (isset($_POST['remember'])) 
				{ // if set to remember create hash
					$cookie_id = $username . ";" . sha1($username.$secret_key); // not as secure for session
					$query = "UPDATE registrationdb SET lastlog='$cookie_id' WHERE username='$username'";
					if(mysqli_query($conn, $query))
					{
						setcookie('username', $cookie_id, time()+60);
					}
					else
					{
						echo "error";
					}
				}

				//Collect favorites
				$query = "SELECT * FROM favorites WHERE username LIKE '$username'"; // get username from db
				$results = mysqli_query($conn, $query);
				if ($results) 
				{
					if ($results->num_rows > 0) 
					{
						echo " || GETTING FAVS";
						$favorites = array();
						while ($row = $results->fetch_assoc())
				        {
				           array_push($favorites, $row['videoID']);
				        }
				        $_SESSION['favorites'] = $favorites;
						print_r ($favorites);
					}
				}

				header('Location: favorites.php');
			}
			else
			{
				$status = 1; // bad password
			}
		}
		else 
		{
			$status = 1; // otherwise failure (no such user)
		}
	}
	else 
	{
		echo "Login fail!";
		$status = 1; // otherwise failure (no such user)
	}
}
?>

<html>
<head>
	<meta charset="UTF-8">
	<title>Login to our website | Team 5 | Martial Arts</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div id="header">
		<div class="section">
			<div class="logo">
				<a href="index.php">shaolin</a>
			</div>
			<nav>
				<ul>
					<li>
						<a href="submission.php">SUBMIT</a>
					</li>
					<li>
						<a href="#">CATEGORY</a>
							<ul>
							<li><a href="category.php?cat=Tutorial">Tutorials</a></li>
							<li><a href="category.php?cat=Entertainment">Entertainment</a></li>
							<li><a href="category.php?cat=Application">Application</a></li>
							<li><a href="category.php?cat=Weapon">Weapons</a></li>
							<li><a href="category.php?cat=Group demo">Group demos</a></li>
							<li><a href="category.php?cat=Others">Others</a></li>
							</ul>
					</li>
					<li>
						<a href="#">videos</a>
							<ul>
							<li><a href="list.php">List videos</a></li>
							<li><a href="search.php">Search videos</a></li>
							<li><a href="update_entry.php">Update videos</a></li>
							</ul>
					</li>
					<li class="selected">
						<a href="#">Profile</a>
							<ul>
							<li><a href="register.php">Register</a></li>
							<?php
								echo "<li><a href=\"favorites.php\">Favorites</a></li>";
								if (isset($_SESSION['loggedIn'])) {
									echo "<li><a href=\"logout.php\">Logout</a></li>";
								}
								else
									echo "<li><a href=\"login.php\">Login</a></li>";
							?>
							</ul>
					</li>
					<li>
						<a href="about.php">about</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div id="body">
		<?php
			if ($status == 0)
				echo "You logged in successfully! Welcome " . $_SESSION['user'] . "!<br><br><br>";
			else if ($status == 1)
				echo "There was something wrong with your credentials. Check your username and password!<br><br><br>";
			if ($status != 0) {
				echo "
				<h3>Login:</h3>
				<form method=post id=\"daform\" action=\"login.php\">
				<b>Username:</b>
				<input type=\"email\" size=20 name=username "; if ($status == 1) echo "placeholder=\"$username\""; else echo "placeholder=\"20 characters max\""; echo " required><br/>
				<br>
				<b>Password:</b>
				<input type=password size=20 name=password pattern=\".{8,20}\" required><br/>
				<input type=\"checkbox\" name=\"remember\" value=TRUE>Remember me
				<br><br>
				<input type=submit name=submit value=\"Submit\">
				<input type=reset name=reset value=\"Reset\"></form>";
			}
			else
				echo "You are already logged in " . $_SESSION['user'] ."!";
		?>
	</div>
	<div id="footer">
		<div>
			<div class="connect">
				<a href="http://twitter.com/#!/sjsu" id="twitter">twitter</a>
				<a href="http://www.facebook.com/sanjosestate" id="facebook">facebook</a>
				<a href="http://pinterest.com/sjsu/" id="pinterest">pinterest</a>
			</div>
			<p>
				&copy; copyright 2014 | all rights reserved.
			</p>
		</div>
	</div>
</body>
</html>