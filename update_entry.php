<?php 
include("dbconnect.php");

	  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE);

session_start();

$auth = false;
$srch = false;

if (isset($_SESSION['user']))
	if ($_SESSION['user'] === "admin@admin.com")
		$auth = true;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$title = $_POST['video'];
	$link = $_POST['link'];
	$length = $_POST['length'];
	$quality = $_POST['quality'];
	$desc = htmlspecialchars($_POST['desc']);
	$language = $_POST['lang'];
	$views = $_POST['views'];
	$type = $_POST['cat'];
	$thumbnail = $_POST['thumbnail'];
	$keywords = $_POST['tags'];
	$offset = $_POST['offset'];
	$id = $_POST['id'];
	if (isset($_POST['search'])) {
		$search = $_POST['search'];
		$srch = true;
	}
	else
		$search = "";
	if (isset($_POST['type']))
		$searchtype = $_POST['type'];
	$page = $_POST['referrer'];
	$tags = "";
	$type = explode(",",$type);
		
	$status = 0;
	
	}
else
	$status = 1;
?>

<html>
<head>
	<meta charset="UTF-8">
	<title>Update a Video</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div id="header">
		<div class="section">
			<div class="logo">
				<a href="index.php">shaolin</a>
			</div>
			<nav>
				<ul>
					<li>
						<a href="submission.php">SUBMIT</a>
					</li>
					<li>
						<a href="#">CATEGORY</a>
							<ul>
							<li><a href="category.php?cat=Tutorial">Tutorials</a></li>
							<li><a href="category.php?cat=Entertainment">Entertainment</a></li>
							<li><a href="category.php?cat=Application">Application</a></li>
							<li><a href="category.php?cat=Weapon">Weapons</a></li>
							<li><a href="category.php?cat=Group demo">Group demos</a></li>
							<li><a href="category.php?cat=Others">Others</a></li>
							</ul>
					</li>
					<li>
						<a href="#">videos</a>
							<ul>
							<li><a href="list.php">List videos</a></li>
							<li><a href="search.php">Search videos</a></li>
							<li><a href="update_entry.php">Update videos</a></li>
							</ul>
					</li>
					<li class="selected">
						<a href="#">Profile</a>
							<ul>
							<li><a href="register.php">Register</a></li>
							<?php
								echo "<li><a href=\"favorites.php\">Favorites</a></li>";
								if (isset($_SESSION['loggedIn'])) {
									echo "<li><a href=\"logout.php\">Logout</a></li>";
								}
								else
									echo "<li><a href=\"login.php\">Login</a></li>";
							?>
							</ul>
					</li>
					<li>
						<a href="about.php">about</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div id="body">
		<?php 

				
			if ($auth) {
				echo "
					<h3>Update a Video</h3>
					<form method=post id=\"daform\" action=\"$page?offset=$offset&cat=$search\">
					<b>Title:</b>
					<input type=text size=80 name=video value=\"$title\" required><br/>
					<br>
					<b>Link:</b>
					<input type=text size=40 name=link value=\"$link\" required><br/>
					<br>
					<b>Video Length:</b>
					<select name=length>";
					// selecting length
					if ($length <= 5)
						echo "<option value=5 selected>0-5m</option>";
					elseif ($length <= 20)
						echo "<option value=20 selected>10-20m</option>";
					elseif ($length <= 40)
						echo "<option value=40 selected>20-40m</option>";
					elseif ($length <=60)
						echo "<option value=60 selected>40-60m</option>";
					else
						echo "<option value=99 selected>60m+</option>";
					echo "</select><br><br>
					<b>Video Quality:</b>
					<select name=\"quality\">";
					// selecting quality
					if ($quality === "144")
						echo "<option value=\"144\" selected>144p</option>";
					elseif ($quality === "240")
						echo "<option value=\"240\" selected>240p</option>";
					elseif ($quality === "360")
						echo "<option value=\"360\" selected>360p</option>";
					elseif ($quality === "480")
						echo "<option value=\"480\" selected>480p</option>";
					elseif ($quality === "720")
						echo "<option value=\"720\" selected>720p</option>";
					elseif ($quality === "1080")
						echo "<option value=\"1080\" selected>1080p</option>";
					else
						echo "<option value=\"9999\" selected>1440p/2K/4K</option>";
					echo "</select><br>
					<br>
					<b>Description:</b></br>
					<textarea name=\"desc\" form=\"daform\" wrap=\"virtual\" cols=\"30\" maxlength=\"1000\">$desc</textarea>
					<br>
					<b>Language:</b>";
					if ($language === "English") {
						echo "<input type=\"radio\" name=\"lang\" value=\"English\" checked><label>English</label>";
						echo "<input type=\"radio\" name=\"lang\" value=\"Non-English\"><label>Non-English</label><br>";
					}
					else {
						echo "<input type=\"radio\" name=\"lang\" value=\"English\"><label>English</label>";
						echo "<input type=\"radio\" name=\"lang\" value=\"Non-English\" checked><label>Non-English</label><br>";
					}
					echo "<br>
					<b>View Count:</b>
					<select name=\"views\">";
					// selecting views
					if ($views <= 75000)
						echo "<option value=75000 selected>50k-70k</option>";
					elseif ($views <= 100000)
						echo "<option value=100000 selected>75k-100k</option>";
					elseif ($views <= 125000)
						echo "<option value=125000 selected>100k-125k</option>";
					elseif ($views <= 150000)
						echo "<option value=150000 selected>125k-150k</option>";
					else
						echo "<option value=999999 selected>150k+</option>";
					echo "</select><br><br>
					<b>Video Type:</b>";
					// checking video categories
					if (in_array("Tutorial", $type))
						echo "<input type=\"checkbox\" name=\"cat[]\" value=\"Tutorial\" checked><label>Tutorial</label>";
					else
						echo "<input type=\"checkbox\" name=\"cat[]\" value=\"Tutorial\"><label>Tutorial</label>";
					if (in_array("Entertainment", $type))
						echo "<input type=\"checkbox\" name=\"cat[]\" value=\"Entertainment\" checked><label>Entertainment</label>";
					else
						echo "<input type=\"checkbox\" name=\"cat[]\" value=\"Entertainment\"><label>Entertainment</label>";
					if (in_array("Application", $type))
						echo "<input type=\"checkbox\" name=\"cat[]\" value=\"Application\" checked><label>Application</label><br>";
					else
						echo "<input type=\"checkbox\" name=\"cat[]\" value=\"Application\"><label>Application</label><br>";
					if (in_array("Weapon", $type))
						echo "<input type=\"checkbox\" name=\"cat[]\" value=\"Weapon\" checked><label>Weapon</label>";
					else
						echo "<input type=\"checkbox\" name=\"cat[]\" value=\"Weapon\"><label>Weapon</label>";
					if (in_array("Group demo", $type))
						echo "<input type=\"checkbox\" name=\"cat[]\" value=\"Group demo\" checked<label>Group demo</label>";
					else
						echo "<input type=\"checkbox\" name=\"cat[]\" value=\"Group demo\"<label>Group demo</label>";
					if (in_array("Other", $type))
						echo "<input type=\"checkbox\" name=\"cat[]\" value=\"Others\" checked<label>Others</label><br>";
					else
						echo "<input type=\"checkbox\" name=\"cat[]\" value=\"Others\"<label>Others</label><br>";
					
					echo "<br><b>Thumbnail:</b>
					<input type=text size=40 name=thumbnail value=\"$thumbnail\"><br/>
					<br><br><b>Tags</b> (separate with commas)</br>
					<textarea name=\"tags\" form=\"daform\" rows=\"3\" maxRows=\"3\" wrap=\"virtual\" cols=\"30\" maxlength=\"100\">$keywords</textarea>
					<br>

					<br>
					<input type=\"hidden\" name=\"update\" value=\"true\">
					<input type=\"hidden\" name=\"offset\" value=\"$offset\">
					<input type=\"hidden\" name=\"id\" value=$id>";
					if ($srch) {
						echo "<input type=\"hidden\" name=\"search\" value=\"$search\">
							<input type=\"hidden\" name=\"type\" value=\"$searchtype\">
							<input type=\"hidden\" name=\"martialarts\" value=\"{$_POST['martialarts']}\">";
					}
					echo "<input type=submit name=submit value=\"Submit\">
					<input type=reset name=reset value=\"Reset\">

					</form>";
				}
				else
					echo "<h3>You do not have access to view this page!</h3>";
		?>
	</div>
	<div id="footer">
		<div>
			<div class="connect">
				<a href="http://twitter.com/#!/sjsu" id="twitter">twitter</a>
				<a href="http://www.facebook.com/sanjosestate" id="facebook">facebook</a>
				<a href="http://pinterest.com/sjsu/" id="pinterest">pinterest</a>
			</div>
			<p>
				&copy; copyright 2014 | all rights reserved.
			</p>
		</div>
	</div>
</body>
</html>