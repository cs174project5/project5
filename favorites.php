<?php
	include("dbconnect.php");
	session_start();
	

	
if (isset($_SESSION['loggedIn'])) {
	if ($_SESSION['loggedIn']) {
		$loggedIn = true;
		
		if (isset($_SESSION['favorites']))
		{
			$favorites = $_SESSION['favorites'];
		}
	}
}

	// Check if the form has been submitted:
	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		if (isset($_POST['update'])) {
			if ($_POST['update']) {
				$title = $_POST['video'];
				$link = $_POST['link'];
				$length = $_POST['length'];
				$quality = $_POST['quality'];
				$desc = htmlspecialchars($_POST['desc']);
				$language = $_POST['lang'];
				$views = $_POST['views'];
				$type = $_POST['cat'];
				$thumbnail = $_POST['thumbnail'];
				$keywords = $_POST['tags'];
				$offset = $_POST['offset'];
				$id = $_POST['id'];
				$tags = "";
				if (!empty($type)) {
					foreach($type as $selected) {
						$tags .= $selected . ",";
					}
				}	
				$query = "UPDATE fun_video SET title='$title',videolink='$link',videolength='$length',highestresolution='$quality',
					description='$desc',language='$language',viewcount='$views',videotype='$tags',iconimage='$thumbnail',tag='$keywords' WHERE id=$id";
				if(!(mysqli_query($conn,$query)))
					echo $conn->error;
				$offset = $_POST['offset'];
			}
		}		
				
		
		if ( isset($_POST['add_favorite']) )
		{
			$videoID = $_POST['add_favorite'];
			$query = "INSERT INTO favorites (username, videoID) VALUES ('$username', '$videoID');";
			if(mysqli_query($conn, $query))
			{
				array_push($favorites, $_POST['add_favorite']);
			}
			else
			{
				echo $conn->error;
			}
		}
		else if ( isset($_POST['remove_favorite']) )
		{
			$videoID = $_POST['remove_favorite'];
			$query = "DELETE FROM favorites WHERE username='$username' AND videoID='$videoID';";
			if(mysqli_query($conn, $query))
			{
				$favorites = array_diff($favorites, array($videoID));
			}
			else
			{
				echo "unable to remove from favorites";
			}
		}
		$_SESSION['favorites'] = $favorites;
	}
?>

<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Martial Arts Video List</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" rel="stylesheet" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
	<script src="scripts/youtubepopup.js"></script>
	<script src="scripts/tablesorter.js"></script>
	<script src="scripts/youtubepopup.js"></script>
	<script>
		$(document).ready(function() 
			{ 
				$("#myTable").tablesorter({
					headers: {
						0: {
							sorter: false
						}
					}
				}); 
			} 
		); 
	</script>
	<script type="text/javascript">
		$(function () {
			$("a.youtube").YouTubePopup({ autoplay: 1,
					hideTitleBar: true,
					theme: 'dark'});
		});
    </script>
</head>
<body>
	<div id="header">
		<div class="section">
			<div class="logo">
				<a href="index.php">shaolin</a>
			</div>
			<nav>
				<ul>
					<li>
						<a href="submission.php">SUBMIT</a>
					</li>
					<li>
						<a href="#">CATEGORY</a>
							<ul>
							<li><a href="category.php?cat=Tutorial">Tutorials</a></li>
							<li><a href="category.php?cat=Entertainment">Entertainment</a></li>
							<li><a href="category.php?cat=Application">Application</a></li>
							<li><a href="category.php?cat=Weapon">Weapons</a></li>
							<li><a href="category.php?cat=Group demo">Group demos</a></li>
							<li><a href="category.php?cat=Others">Others</a></li>
							</ul>
					</li>
					<li>
						<a href="#">videos</a>
							<ul>
							<li><a href="list.php">List videos</a></li>
							<li><a href="search.php">Search videos</a></li>
							<li><a href="update_entry.php">Update videos</a></li>
							</ul>
					</li>
					<li class="selected">
						<a href="#">Profile</a>
							<ul>
							<li><a href="register.php">Register</a></li>
							<?php
								echo "<li><a href=\"favorites.php\">Favorites</a></li>";
								if (isset($_SESSION['loggedIn'])) {
									echo "<li><a href=\"logout.php\">Logout</a></li>";
								}
								else
									echo "<li><a href=\"login.php\">Login</a></li>";
							?>
							</ul>
					</li>
					<li>
						<a href="about.php">about</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div class="listing">
		<?php
			if (isset($_SESSION['loggedIn']))
				if ($_SESSION['loggedIn'])
					echo "<h3>".$_SESSION['user']."'s Favorite Videos</h3>";
		?>
		<table id="myTable" class="tablesorter"> 
		<thead>
		<?php
			if ($loggedIn) {
			
				echo "<a href=\"profile.php\">Change your password</a><br><br>";
				echo "
				<tr> 
					<th>Video</th> 
					<th>Title</th> 
					<th>Description</th> 
					<th>Category</th> 
					<th>Length</th>
					<th>Resolution</th>
					<th>Language</th>
					<th>View Count</th>
					<th>Tags</th>";
					if ($username === "admin@admin.com")
						echo "<th>edit</th>";
				echo "<th>Favorites</th>
				<th>Type</th>
				</tr> 
				</thead>
				<tbody>"; // headers

				if (isset($_GET['offset'])) {
					$index = $_GET['index'];
					$offset = $_GET['offset'];
				}
				else {
					$index = 0;
					$offset = 0;
				}

				$size = count($favorites);
				if ($size < 10)
					$limit = $size; 
				else
					$limit = $index+10;
					
				//foreach ($favorites as $key => $id) 
				while ($index < $limit)
				{
					$id = $favorites[$index];
					$query = "SELECT * FROM fun_video WHERE id LIKE '$id'";
					$results = mysqli_query($conn, $query);
					
					if ($results->num_rows > 0) {
						while ($row = $results->fetch_assoc()) {
						
							echo "<tr><td>";
								if ($row["iconimage"] != null)
									echo "<a class=\"youtube\" href=\"". $row["videolink"]."\"><img src=\"".$row['iconimage']."\"></a>";
								else
									echo "<a class=\"youtube\" href=\"".$row["videolink"]."\"><img src=\"images/no_img.png\"></a>";
							echo "</td><td>".$row["title"]."</td>";
							echo "<td>".$row["description"]."</td>";
							echo "<td>".$row["videotype"]."</td>";
							
							if ($row["videolength"] <= 10) {
								$str = "0-10 minutes"; 
							}
							else if ($row["videolength"] <= 20) {
								$str = "10-20 minutes";
							}
							else if ($row["videolength"] <= 40) {
								$str = "20-40 minutes";
							}
							else if ($row["videolength"] <= 60) {
								$str = "40-60 minutes";
							}
							else {
								$str = "Over 60 minutes";
							}
			
							echo "<td>$str</td>";
							echo "<td>".$row["highestresolution"]."</td>";
							echo "<td>".$row["language"]."</td>";

							if ($row["viewcount"] <= 75000) {
								$str = "50-70k"; 
							}
							else if ($row["viewcount"] <= 100000) {
								$str = "75-100k";
							}
							else if ($row["viewcount"] <= 125000) {
								$str = "100-125k";
							}
							else if ($row["viewcount"] <= 150000) {
								$str = "125-150k";
							}
							else {
								$str = "Over 150k";
							}
							echo "<td>$str</td>";
							echo "<td>".$row["tag"]."</td>";

							if ($loggedIn && ($username === "admin@admin.com")) {
								echo "<td>
									<form method=\"post\" action=\"update_entry.php\">
										<input type=\"hidden\" name=\"video\" value=\"{$row['title']}\">
										<input type=\"hidden\" name=\"link\" value=\"{$row['videolink']}\">
										<input type=\"hidden\" name=\"length\" value=\"{$row['videolength']}\">
										<input type=\"hidden\" name=\"quality\" value=\"{$row['highestresolution']}\">
										<input type=\"hidden\" name=\"desc\" value=\"{$row['description']}\">
										<input type=\"hidden\" name=\"lang\" value=\"{$row['language']}\">
										<input type=\"hidden\" name=\"views\" value=\"{$row['viewcount']}\">
										<input type=\"hidden\" name=\"cat\" value=\"{$row['videotype']}\">
										<input type=\"hidden\" name=\"thumbnail\" value=\"{$row['iconimage']}\">
										<input type=\"hidden\" name=\"tags\" value=\"{$row['tag']}\">
										<input type=\"hidden\" name=\"offset\" value=\"$offset\">
										<input type=\"hidden\" name=\"id\" value={$row['id']}>
										<input type=\"hidden\" name=\"referrer\" value=\"favorites.php\">
										<input type=\"submit\" value=\"edit\">
									</form>
								</td>
								";
							}
							
							if ($loggedIn)
							{
								if ( count($favorites) > 0 )
								{
									$found = false;
									foreach ($favorites as $key => $value)
									{
										if ($row['id'] == $value)
										{
											$found = true;
											echo "<td>
													<!-- <button type=\"button\">remove from favorites</button> -->
													<form method=\"post\" action=\"\">
														<input type=\"hidden\" name=\"remove_favorite\" value=\"{$row['id']}\"> 
														<input type=\"submit\" value=\"remove from favorites\">
													</form>
												</td>";
												break;
										}
									}

									if (!$found) {
										echo "<td>
												<!-- <button type=\"button\">add to favorites</button> -->
												<form method=\"post\" action=\"\">
													<input type=\"hidden\" name=\"add_favorite\" value=\"{$row['id']}\"> 
													<input type=\"submit\" value=\"add to favorites\">
												</form>
										</td>";
									}
									
								}
								else
								{
									echo "<td>
											<!-- <button type=\"button\">add to favorites</button> -->
											<form method=\"post\" action=\"\">
												<input type=\"hidden\" name=\"add_favorite\" value=\"{$row['id']}\"> 
												<input type=\"submit\" value=\"add to favorites\">
											</form>
										</td>";
								}
							}
							echo "<td>".$row["category"]."</td>";
							echo "</tr>";
							$flagged = $flagged + 1;	
						}			
					}
					$index++;
				}
				echo "</tbody></table>";
				if($flagged != 0) {
					$previous = $offset-10;
					$pindex = $index-20;
					if ($offset != 0)
						echo "<a href=\"favorites.php?offset=$previous&index=$pindex\"><< PREVIOUS</a>&nbsp;";
					$offset += 10;
					if ($flagged == 10)
						echo "<a href=\"favorites.php?offset=$offset&index=$index\">NEXT >></a>";
				}
			}
			else
				echo "<h1>You must be logged in to see this page!</h1>";
		?>
		
	</div>
	<div id="footer">
		<div>
			<div class="connect">
				<a href="http://twitter.com/#!/sjsu" id="twitter">twitter</a>
				<a href="http://www.facebook.com/sanjosestate" id="facebook">facebook</a>
				<a href="http://pinterest.com/sjsu/" id="pinterest">pinterest</a>
			</div>
			<p>
				&copy; copyright 2014 | all rights reserved.
			</p>
		</div>
	</div>
</body>
</html>