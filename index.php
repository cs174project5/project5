<?php
		  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE);

	include("dbconnect.php");
	session_start();
	
if (isset($_SESSION['loggedIn'])) {
	if($_SESSION['loggedIn']) {	// check for login session
		$username = $_SESSION['user'];
		$favorites = $_SESSION['favorites'];
	}
}
if (isset($_COOKIE['username'])) {
	if ($_COOKIE['username'] != null) {
		$cookie = $_COOKIE['username'];
		$session = explode(";", $cookie);
		$query = "SELECT * FROM registrationdb WHERE username LIKE '$username'"; // get username from db
		$results = mysqli_query($conn, $query);
		if ($results) {
			$user = $results->fetch_assoc();
			if($session[1] === sha1($session[0].$secret_key)) { // check if cookie is valid
				$_SESSION['user'] = $session[0]; // set session to username
				$username = $_SESSION['user'];
				$status = 0; //disable login
				$_SESSION['loggedIn'] = true;
				echo "OK";
				//Collect favorites
				$query = "SELECT * FROM favorites WHERE username LIKE '$username'"; // get username from db
				$results = mysqli_query($conn, $query);
				if ($results) 
				{
					if ($results->num_rows > 0) 
					{
						$favorites = array();
						while ($row = $results->fetch_assoc()) { array_push($favorites, $row['videoID']); }
						$_SESSION['favorites'] = $favorites;
					}
				}
			}
			else {
				echo "fail"; // invalid cookie, remove it
				unset($_COOKIE['username']);
				setcookie('username',null,time()-3600);
			}
		}
	}
}

?>
<!DOCTYPE HTML>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Team 5: Martial Arts</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" rel="stylesheet" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
	<script src="scripts/jssor.slider.mini.js"></script>
	<script src="scripts/youtubepopup.js"></script>
	<script type="text/javascript">
		$(function () {
			$("a.youtube").YouTubePopup({ autoplay: 1,
					hideTitleBar: true			});
		});
    </script>
</head>
<body>

	<div id="header">
		<div class="section">
			<div class="logo">
				<a href="index.html">shaolin</a>
			</div>
			<nav>
				<ul>
					<li>
						<a href="submission.php">SUBMIT</a>
					</li>
					<li>
						<a href="#">CATEGORY</a>
							<ul>
							<li><a href="category.php?cat=Tutorial">Tutorials</a></li>
							<li><a href="category.php?cat=Entertainment">Entertainment</a></li>
							<li><a href="category.php?cat=Application">Application</a></li>
							<li><a href="category.php?cat=Weapon">Weapons</a></li>
							<li><a href="category.php?cat=Group demo">Group demos</a></li>
							<li><a href="category.php?cat=Others">Others</a></li>
							</ul>
					</li>
					<li>
						<a href="#">videos</a>
							<ul>
							<li><a href="list.php">List videos</a></li>
							<li><a href="search.php">Search videos</a></li>
							<li><a href="update_entry.php">Update videos</a></li>
							</ul>
					</li>
					<li class="selected">
						<a href="#">Profile</a>
							<ul>
							<li><a href="register.php">Register</a></li>
							<?php
								echo "<li><a href=\"favorites.php\">Favorites</a></li>";
								if (isset($_SESSION['loggedIn'])) {
									echo "<li><a href=\"profile.php\">$username</a>";
									echo "<li><a href=\"logout.php\">Logout</a></li>";
								}
								else
									echo "<li><a href=\"login.php\">Login</a></li>";
							?>
							</ul>
					</li>
					<li>
						<a href="about.php">about</a>
					</li>
				</ul>
			</nav>
		</div>
			<br><br>
			<form method="get" action="search.php" id="search">
				<input type="text" name="search" placeholder="Enter Your Search Terms"/>
				<input type="hidden" name="offset" value=0 />
				<input type="hidden" name="type" value="title" />
				<input type="submit" value="Search"/>
			</form>
		<div class="article">
			<div id="player"></div>
			    <script>
				  var tag = document.createElement('script');

				  tag.src = "https://www.youtube.com/iframe_api";
				  var firstScriptTag = document.getElementsByTagName('script')[0];
				  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

				  var i = 0;
				  var item = [];
				  item.push("h0VAH3tUQsg");
				  item.push("DUmaFgzD2nk");
				  item.push("BxvgMHezVt8");
				  var player;
				  function onYouTubeIframeAPIReady() {
					player = new YT.Player('player', {
					  height: '405',
					  width: '940',
					  videoId: 'h0VAH3tUQsg',
					  playerVars: { 'controls': 0 },
					  events: {
						'onReady': onPlayerReady,
						'onStateChange': onPlayerStateChange
					  }
					});
				  }
				  
				  function onPlayerReady(event) {
					event.target.playVideo();
					event.target.mute();
				  }

				  function onPlayerStateChange(event) {
					if (event.data == YT.PlayerState.PLAYING) {
					  setTimeout(stopVideo, 15000);
					}
				  }
				  function stopVideo() {
					player.stopVideo();
					i++;
					if (i >= item.length-1) {
						i = 0;
					}
					player.loadVideoById(item[i]);
				  }
				</script>
		</div>
	</div>
	<div id="body">
		<?php
			$dupe_check = array();
			if (isset($_SESSION['loggedIn'])) 
			{
				if ($_SESSION['loggedIn'] && count($favorites)>0) {
					echo "<h1>Favorite Videos</h1>";
					echo "<ul>";
					if (count($favorites) >= 4)
						$limit = 4;
					else
						$limit = count($favorites);
					$i = 0;
					while ($i < $limit) {
						$selection = rand(0, count($favorites)-1); // get random number
						if(!(in_array($selection, $dupe_check))) { // check if video already used
							$dupe_check[] = $selection;
							$query = "SELECT * FROM fun_video WHERE id LIKE '$favorites[$selection]'";
							$results = mysqli_query($conn, $query);
							while ($row = $results->fetch_assoc()) {
								if ($row["iconimage"] != null)
									echo "<li><a class=\"youtube\" href=\"". $row["videolink"]."\"><img src=\"".$row['iconimage']."\"height=\"170\" width=\"210\"></a>";
								else
									echo "<li><a class=\"youtube\" href=\"".$row["videolink"]."\"><img src=\"images/no_img.png\" height=\"170\" width=\"210\"></a>";
								echo "<b>".$row["title"]."</b>";
								if (strlen($row["description"]) > 200)
									echo "<p>".substr($row["description"],0,200)." ...</p></li>";
								else
									echo "<p>".$row["description"]."</p></li>";
								$i = $i + 1;
							}
						}

					}
					echo "</ul><ul>";
				}
				else {
					$status = true;
					echo "<h1>Featured Videos</h1><ul>";

					if ($result = $conn->query("SELECT * FROM fun_video ORDER BY id")) {
						$num_records = $result->num_rows;
						$result->close();
					}
					if ($num_records < 8)
						$limit = $num_records;
					else
						$limit = 8;
						
					$i = 0;
					while ($i < $limit) {
						$selection = rand(0,$num_records-1);
						if (!(in_array($selection, $dupe_check))) {
							$query = "SELECT * FROM fun_video WHERE id LIKE $selection";
							$results = mysqli_query($conn, $query);
							if ($results) {
								while ($row = $results->fetch_assoc()) {
									if ($row != null) {
										$dupe_check[] = $selection;
										if ($row["iconimage"] != null)
											echo "<li><a class=\"youtube\" href=\"". $row["videolink"]."\"><img src=\"".$row['iconimage']."\"height=\"170\" width=\"210\"></a>";
										else
											echo "<li><a class=\"youtube\" href=\"".$row["videolink"]."\"><img src=\"images/no_img.png\" height=\"170\" width=\"210\"></a>";
										echo "<b>".$row["title"]."</b>";
										if (strlen($row["description"]) > 200)
											echo "<p>".substr($row["description"],0,200)." ...</p></li>";
										else
											echo "<p>".$row["description"]."</p></li>";
										$i++;
										if (($i%4) == 0)
											echo "<br><br></ul><ul>";
									}
								}
							}
						}
					}
				}
			}
			else {
				$status = true;
				echo "<h1>Featured Videos</h1><ul>";

				if ($result = $conn->query("SELECT * FROM fun_video ORDER BY id")) {
					$num_records = $result->num_rows;
					$result->close();
				}
				if ($num_records < 4)
					$limit = $num_records;
				else
					$limit = 8;
					
				$i = 0;
				while ($i < $limit) {
					$selection = rand(0,$num_records-1);
					if (!(in_array($selection, $dupe_check))) {
						$query = "SELECT * FROM fun_video WHERE id LIKE $selection";
						$results = mysqli_query($conn, $query);
						if ($results) {
							while ($row = $results->fetch_assoc()) {
								if ($row != null) {
									$dupe_check[] = $selection;
									if ($row["iconimage"] != null)
										echo "<li><a class=\"youtube\" href=\"". $row["videolink"]."\"><img src=\"".$row['iconimage']."\"height=\"170\" width=\"210\"></a>";
									else
										echo "<li><a class=\"youtube\" href=\"".$row["videolink"]."\"><img src=\"images/no_img.png\" height=\"170\" width=\"210\"></a>";
									echo "<b>".$row["title"]."</b>";
									if (strlen($row["description"]) > 200)
										echo "<p>".substr($row["description"],0,200)." ...</p></li>";
									else
										echo "<p>".$row["description"]."</p></li>";
									$i++;
									if (($i%4) == 0)
										echo "<br><br></ul><ul>";
								}
							}
						}
					}
				}
			}
				
			?>

		</ul>
		<div>
			<div class="featured">
				<h2>What is Shaolin?</h2>
				<img src="" alt="">
				<h3>Styles</h3>
				<p>
				<ul>	
				<li>Arhat's 18 hands (罗汉十八手): known as the oldest style.</li>
				<li>Flood style (洪拳): with the small form (小洪拳) known as the mother the 18 styles, and the big form (大洪拳) known as the origin of all the styles,</li>
				<li>Explosive style (炮拳): known as the king of the styles,</li>
				<li>Through-the-Arms style (通臂拳),</li>
				<li>7-star & Long Guard the Heart and Mind Gate style (七星 & 长护心意门拳),</li>
				<li>Plum Blossom style (梅花拳),</li>
				<li>Facing&Bright Sun style (朝&昭 阳拳),</li>
				<li>Arhat style (罗汉拳): known as the most representative style,</li>
				<li>Vajrapani style (金刚拳),</li>
				<li>Emperor's Long-range style (太祖长拳): known as the most graceful style,</li>
				<li>Guard the Home, also means Special, style (看家拳),</li>
				<li>Chain Hands and Short-range combat (连手短打),</li>
				<li>6-Match style (六合拳),</li>
				<li>Soft style (柔拳),</li>
				<li>Mind (心意拳) aka Confusing Path style (迷踪拳),</li>
				<li>Imitative styles (象形拳),</li>
				<li>Drunken style (醉拳), which is an imitative style</li>
				</ul>
				</p>
				<img src="" alt="">
				<h3>Brief</h3>
				<p>
					Believed to be the oldest institutionalized style of kung fu and is one of the most famous martial arts. Shaolin kung fu originated and was developed in the Buddhist Shaolin temple in Songshan mountain, Henan province, China. During the 1500 years of the development of Shaolin kung fu, it became one of the biggest schools of kung fu with abundant contents and various barehanded and weapon styles. Besides, a majority of other kung fu styles claim to have been created or inspired on the base of Shaolin kung fu. One Chinese saying is: "All martial arts under heaven arose out of Shaolin."
				</p>
			</div>

		</div>
	</div>
	<div id="footer">
		<div>
			<div class="connect">
				<a href="http://twitter.com/#!/sjsu" id="twitter">twitter</a>
				<a href="http://www.facebook.com/sanjosestate" id="facebook">facebook</a>
				<a href="http://pinterest.com/sjsu/" id="pinterest">pinterest</a>
			</div>
			<p>
				&copy; copyright 2014 | all rights reserved.
			</p>
		</div>
	</div>
</body>
</html>