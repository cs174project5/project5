<?php
include("dbconnect.php");
session_start();
$status = 0;

//if (session_status() == PHP_SESSION_ACTIVE && isset($_SESSION['loggedIn'])) {
//if (isset($_SESSION['loggedIn']) {
if (isset($_SESSION['loggedIn'])) {
	if($_SESSION['loggedIn']) {	// check for session
		session_unset();
		unset($_COOKIE['username']);
		setcookie('username',null,time()-3600);
		session_destroy();
		$status = 1;
	}
}
elseif (isset($_COOKIE['username'])) {
	if ($_COOKIE['username'] != null) {
		session_unset();
		unset($_COOKIE['username']);
		setcookie('username',null,time()-3600);
		session_destroy();
		$status = 1;
	}
}
header('Location: index.php');
?>

<html>
<head>
	<meta charset="UTF-8">
	<title>Login to our website</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div id="header">
		<div class="section">
			<div class="logo">
				<a href="index.php">shaolin</a>
			</div>
			<nav>
				<ul>
					<li>
						<a href="submission.php">SUBMIT</a>
					</li>
					<li>
						<a href="#">CATEGORY</a>
							<ul>
							<li><a href="category.php?cat=Tutorial">Tutorials</a></li>
							<li><a href="category.php?cat=Entertainment">Entertainment</a></li>
							<li><a href="category.php?cat=Application">Application</a></li>
							<li><a href="category.php?cat=Weapon">Weapons</a></li>
							<li><a href="category.php?cat=Group demo">Group demos</a></li>
							<li><a href="category.php?cat=Others">Others</a></li>
							</ul>
					</li>
					<li>
						<a href="#">videos</a>
							<ul>
							<li><a href="list.php">List videos</a></li>
							<li><a href="search.php">Search videos</a></li>
							<li><a href="update_entry.php">Update videos</a></li>
							</ul>
					</li>
					<li class="selected">
						<a href="#">Profile</a>
							<ul>
							<li><a href="register.php">Register</a></li>
							<?php
								echo "<li><a href=\"favorites.php\">Favorites</a></li>";
								if (isset($_SESSION['loggedIn'])) {
									echo "<li><a href=\"logout.php\">Logout</a></li>";
								}
								else
									echo "<li><a href=\"login.php\">Login</a></li>";
							?>
							</ul>
					</li>
					<li>
						<a href="about.php">about</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div id="body">
		<?php
			if ($status == 1)
				echo "You logged out successfully! Come back soon!<br><br>";
			else
				echo "You aren't even logged in!<br><br>";
		?>
	</div>
	<div id="footer">
		<div>
			<div class="connect">
				<a href="http://twitter.com/#!/sjsu" id="twitter">twitter</a>
				<a href="http://www.facebook.com/sanjosestate" id="facebook">facebook</a>
				<a href="http://pinterest.com/sjsu/" id="pinterest">pinterest</a>
			</div>
			<p>
				&copy; copyright 2014 | all rights reserved.
			</p>
		</div>
	</div>
</body>
</html>