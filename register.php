<?php
include("dbconnect.php");

$registered = false;
$error = false;
$status = -1;

// Check if the form has been submitted:
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$username = htmlspecialchars($_POST['username']);
	$password = htmlspecialchars($_POST['password']);

	$password_encrypt = sha1($password.$secret_key);

	date_default_timezone_set('US/Pacific');
	$currentTime = time();



		// prepare statement
		if ( !($stmt = $conn->prepare("INSERT INTO registrationdb (username, password, lastlog) VALUES (?, ?, ?)")) ) {
			$error = true;
			echo "<span style=\"color:red\">Prepare failed: (" . $conn->errno . ") " . $conn->error . "</span>";    	
		}

		// bind parameters
		if ( !$stmt->bind_param("ssi", $username, $password_encrypt, $currentTime) ) {
			$error = true;
			echo "<span style=\"color:red\">Prepare failed: (" . $stmt->errno . ") " . $stmt->error . "</span>";
		}

		// execute statement
		if ( !$stmt->execute() ) {
			$error = true;
			$status = 1;
			echo "<span style=\"color:red\">Prepare failed: (" . $stmt->errno . ") " . $stmt->error . "</span>";
		}




	//If no errors (successful registration)
	if (!$error)
	{
		$registered = true;
		session_start();
		$_SESSION['user'] = $username;
		$_SESSION['loggedIn'] = true;
		$_SESSION['timeLoggedIn'] = $currentTime;
		$_SESSION['favorites'] = array();
	}
	else
	{
		echo "<span style=\"color:red\">Unable to register</span>";
	}
}

/*if (isset($_POST['submit'])) {
	
	$username = $_POST['username'];
	$password = $_POST['password'];
	
	//$password_encrypt = password_hash($password, PASSWORD_DEFAULT); // security always (kind of)!
	// NOT SUPPORTED IN PROF WEBSERVER!!
	$password_encrypt = sha1($password.$secret_key);
	
	$query = "insert into registrationdb "
		." (username,password) values "
		."('$username','$password_encrypt')"
		;
		if (mysqli_query($conn,$query))
			$status = 0; // registration success
		else
			$status = 1; // otherwise failure (dupe username)
	}
*/
?>

<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Register to our website</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div id="header">
		<div class="section">
			<div class="logo">
				<a href="index.php">shaolin</a>
			</div>
			<nav>
				<ul>
					<li>
						<a href="submission.php">SUBMIT</a>
					</li>
					<li>
						<a href="#">CATEGORY</a>
							<ul>
							<li><a href="category.php?cat=Tutorial">Tutorials</a></li>
							<li><a href="category.php?cat=Entertainment">Entertainment</a></li>
							<li><a href="category.php?cat=Application">Application</a></li>
							<li><a href="category.php?cat=Weapon">Weapons</a></li>
							<li><a href="category.php?cat=Group demo">Group demos</a></li>
							<li><a href="category.php?cat=Others">Others</a></li>
							</ul>
					</li>
					<li>
						<a href="#">videos</a>
							<ul>
							<li><a href="list.php">List videos</a></li>
							<li><a href="search.php">Search videos</a></li>
							<li><a href="update_entry.php">Update videos</a></li>
							</ul>
					</li>
					<li class="selected">
						<a href="#">Profile</a>
							<ul>
							<li><a href="register.php">Register</a></li>
							<?php
								echo "<li><a href=\"favorites.php\">Favorites</a></li>";
								if (isset($_SESSION['loggedIn'])) {
									echo "<li><a href=\"logout.php\">Logout</a></li>";
								}
								else
									echo "<li><a href=\"login.php\">Login</a></li>";
							?>
							</ul>
					</li>
					<li>
						<a href="about.php">about</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div id="body">
		<?php
			if ($registered == true) {
				echo "Your registration was successful! Thanks $username!<br><br><br>";
			}
		?>
		<h3>Register:</h3>
		<form method="post" id="daform" action="register.php">
		<b>Username:</b> 
		<!-- <input type="text" size="35" name="username" pattern="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$ " placeholder="email@domain.com" required><br/> -->
		<input type="email" name="username" placeholder="email@domain.com" required /><br/>
		<?php 
			if ($registered == false && $status == 1) { 
				echo "<span class=\"hint\">Username ['$username'] already taken! Please try a new one</span><br><br>"; 
			}
		?>
		<br>
		<b>Password:</b>
		<input type="password" size="35" name="password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" placeholder="minimum 8 characters" required><br/>
		<span class="hint">Requires uppercase, lowercase, and a number or special character.</span>
		<br>
		<br>
		<input type="submit" name="submit" value="Submit">
		<input type="reset" name="reset" value="Reset">

		</form>
	</div>
	<div id="footer">
		<div>
			<div class="connect">
				<a href="http://twitter.com/#!/sjsu" id="twitter">twitter</a>
				<a href="http://www.facebook.com/sanjosestate" id="facebook">facebook</a>
				<a href="http://pinterest.com/sjsu/" id="pinterest">pinterest</a>
			</div>
			<p>
				&copy; copyright 2014 | all rights reserved.
			</p>
		</div>
	</div>
</body>
</html>