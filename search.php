<?php	
	include("dbconnect.php");
	session_start();
	
	if (isset($_POST['search'])) {
		$searchterms = htmlspecialchars($_POST['search']);
		$type = $_POST['type'];
		$quality = $_POST['res'];
		$views = $_POST['views'];
		$lang = $_POST['lang'];
		$martialarts = $_POST['martialarts'];
	}
	else
		$searchterms = "";
		
	if (isset($_GET['search'])) {
		$offset = $_GET['offset'];
		$searchterms = $_GET['search'];
		$type = $_GET['type'];
		if (isset($_GET['res']))
			$quality = $_GET['res'];
		if (isset($_GET['views']))
			$views = $_GET['views'];
		else
			$views = 999999999;
		if (isset($_GET['lang']))
			$lang = $_GET['lang'];
		if (isset($_GET['martialarts']))
			$martialarts = $_GET['martialarts'];
	}
	else {
		$offset = 0;
		$previous = 0;
	}
	$flagged = 0;
	

	if ( isset($_SESSION['user']) )
	{
		$loggedin = true;
		$username = $_SESSION['user'];
		$favorites = $_SESSION['favorites'];
	}
	else
	{
		$loggedin = false;
	}
	
		// Check if the form has been submitted:
	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
	
		if (isset($_POST['update'])) {
			if ($_POST['update']) {
				$title = $_POST['video'];
				$link = $_POST['link'];
				$length = $_POST['length'];
				$quality = $_POST['quality'];
				$desc = htmlspecialchars($_POST['desc']);
				$lang = $_POST['lang'];
				$views = $_POST['views'];
				$vidtype = $_POST['cat'];
				$thumbnail = $_POST['thumbnail'];
				$keywords = $_POST['tags'];
				$offset = $_POST['offset'];
				$id = $_POST['id'];
				$tags = "";
				$searchterms = htmlspecialchars($_POST['search']);
				$type = $_POST['type'];
				$martialarts = $_POST['martialarts'];
				if (!empty($vidtype)) {
					foreach($vidtype as $selected) {
						$tags .= $selected . ",";
					}
				}	
				$query = "UPDATE fun_video SET title='$title',videolink='$link',videolength='$length',highestresolution='$quality',
					description='$desc',language='$lang',viewcount='$views',videotype='$tags',iconimage='$thumbnail',tag='$keywords' WHERE id=$id";
				if(!(mysqli_query($conn,$query)))
					echo $conn->error;
				$offset = $_POST['offset'];
			}
		}	
		if ( isset($_POST['add_favorite']) )
		{
			$videoID = $_POST['add_favorite'];
			$query = "INSERT INTO favorites (username, videoID) VALUES ('$username', '$videoID');";
			if(mysqli_query($conn, $query))
			{
				array_push($favorites, $_POST['add_favorite']);
			}
			else
			{
				echo $conn->error;
			}
		}
		else if ( isset($_POST['remove_favorite']) )
		{
			$videoID = $_POST['remove_favorite'];
			$query = "DELETE FROM favorites WHERE username='$username' AND videoID='$videoID';";
			if(mysqli_query($conn, $query))
			{
				$favorites = array_diff($favorites, array($videoID));
			}
			else
			{
				echo "unable to remove from favorites";
			}
		}
		$_SESSION['favorites'] = $favorites;
	}
?>

<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Search Martial Arts Videos</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" rel="stylesheet" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
	<script src="scripts/youtubepopup.js"></script>
	<script src="scripts/tablesorter.js"></script>
	<script>
		$(document).ready(function() 
			{ 
				$("#myTable").tablesorter({
					headers: {
						0: {
							sorter: false
						}
					}
				}); 
			} 
		); 
	</script>
	<script type="text/javascript">
		$(function () {
			$("a.youtube").YouTubePopup({ autoplay: 1,
					hideTitleBar: true			});
		});
    </script>
</head>
<body>
	<div id="header">
		<div class="section">
			<div class="logo">
				<a href="index.php">shaolin</a>
			</div>
			<nav>
				<ul>
					<li>
						<a href="submission.php">SUBMIT</a>
					</li>
					<li>
						<a href="#">CATEGORY</a>
							<ul>
							<li><a href="category.php?cat=Tutorial">Tutorials</a></li>
							<li><a href="category.php?cat=Entertainment">Entertainment</a></li>
							<li><a href="category.php?cat=Application">Application</a></li>
							<li><a href="category.php?cat=Weapon">Weapons</a></li>
							<li><a href="category.php?cat=Group demo">Group demos</a></li>
							<li><a href="category.php?cat=Others">Others</a></li>
							</ul>
					</li>
					<li>
						<a href="#">videos</a>
							<ul>
							<li><a href="list.php">List videos</a></li>
							<li><a href="search.php">Search videos</a></li>
							<li><a href="update_entry.php">Update videos</a></li>
							</ul>
					</li>
					<li class="selected">
						<a href="#">Profile</a>
							<ul>
							<li><a href="register.php">Register</a></li>
							<?php
								echo "<li><a href=\"favorites.php\">Favorites</a></li>";
								if (isset($_SESSION['loggedIn'])) {
									echo "<li><a href=\"logout.php\">Logout</a></li>";
								}
								else
									echo "<li><a href=\"login.php\">Login</a></li>";
							?>
							</ul>
					</li>
					<li>
						<a href="about.php">about</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div class="listing">
		<h3>Search Martial Arts Videos</h3>
		 <form method=post action="search.php">
		 Search terms:
		<!-- PHP -->
		<?php 
			echo "<input type=\"search\" name=\"search\" value=\"$searchterms\">
				<select name=\"type\">
					<option value=\"title\">Title</option>
					<option value=\"description\">Description</option>
					<option value=\"videotype\">Category</option>
					<option value=\"tag\">Tags</option>
				</select>
				<select name=\"res\">
					<option value=\"\">Quality</option>
					<option value=\"144\">144p</option>
					<option value=\"240\">240p</option>
					<option value=\"360\">360p</option>
					<option value=\"480\">480p</option>
					<option value=\"720\">720p</option>
					<option value=\"1080\">1080p</option>
					<option value=\"9999\">1440p/2K/4K</option>
				</select>
				<select name=\"views\">
					<option value=999999999>Views</option>
					<option value=75000>50k-70k</option>
					<option value=100000>75k-100k</option>
					<option value=125000>100k-125k</option>
					<option value=150000>125k-150k</option>
					<option value=999999>150k+</option>
				</select>
				<select name=\"lang\">
					<option value=\"\">Lang</option>
					<option value=\"English\">English</option>
					<option value=\"Non-English\">Non-English</option>
				</select>
				<select name=\"martialarts\">
					<option value=\"\">Martial arts</option>
					<option value=\"Yang Yaichi\">Yang Taichi</option>
					<option value=\"Chen Taichi\">Chen Taichi</option>
					<option value=\"Sun Taichi\">Sun Taichi</option>
					<option value=\"Wu Taichi\">Wu Taichi</option>
					<option value=\"QiGong\">QiGong</option>
					<option value=\"Shaolin\">Shaolin</option>
					<option value=\"Tae kwon do\">Tae kwon do</option>
					<option value=\"Wing Chun\">Wing Chun</option>
					<option value=\"Aikido\">Aikido</option>
					<option value=\"Judo\">Judo</option>
					<option value=\"KungFu Movie\">KungFu Movie</option>
				</select>
				<input type=submit name=submit value=\"Submit\">
				</form>";	
			if (!(empty($searchterms))) {
				// get lower bounds
				if ($views == 75000) {
					$lower = 50000; 
				}
				elseif ($views == 100000) {
					$lower = 75000;
				}
				elseif ($views == 125000) {
					$lower = 100000;
				}
				elseif ($views == 150000) {
					$lower = 125000;
				}
				else {
					$lower = 0;
				}						
				$query = "SELECT * FROM fun_video WHERE $type ";//LIKE '%$searchterms%' LIMIT 10 OFFSET $offset";
				$keys = explode(' ',$searchterms);
				foreach($keys as $v) {
					$v=trim($v);
					if ($i==0)
						$query .= "LIKE '%$v%'";
					else
						$query .= " AND $type LIKE '%$v%'";
					$i++;
				}
				if($quality !== "" && !empty($quality))
					$query .= " AND highestresolution LIKE '$quality'";
				if($lang !== "" && !empty($lang))
					$query .= " AND language LIKE '%$lang%'";
				if($martialarts !== "" && !empty($martialarts))
					$query .= " AND category LIKE '$martialarts'";
				$query .= " LIMIT 10 OFFSET $offset";
				$results = mysqli_query($conn, $query);
				if (mysqli_num_rows($results)) {
					echo "<h3>Results</h3>
						<table id=\"myTable\" class=\"tablesorter\"> 
						<thead> 
							<tr> 
								<th>Video</th> 
								<th>Title</th> 
								<th>Description</th> 
								<th>Category</th> 
								<th>Length</th>
								<th>Resolution</th>
								<th>Language</th>
								<th>View Count</th>
								<th>Tags</th>";

					if ($loggedin && $username === "admin@admin.com")
						echo "<th>edit</th>";
								
					if ($loggedin) 
						echo "<th>favorites</th>";
					
					echo	"<th>type</th></tr> 
						</thead>
						<tbody>";
					while ($row = $results->fetch_assoc()) {
						// display videos between view range
						if ($row['viewcount'] <= $views && $row['viewcount'] >= $lower)
						{
							echo "<tr><td>";
								if ($row["iconimage"] != null)
									echo "<a class=\"youtube\" href=\"". $row["videolink"]."\"><img src=\"".$row['iconimage']."\"></a>";
								else
									echo "<a class=\"youtube\" href=\"".$row["videolink"]."\"><img src=\"images/no_img.png\"></a>";
							echo "</td><td>".$row["title"]."</td>";
							echo "<td>".$row["description"]."</td>";
							echo "<td>".$row["videotype"]."</td>";
							
							if ($row["videolength"] <= 10) {
								$str = "0-10 minutes"; 
							}
							else if ($row["videolength"] <= 20) {
								$str = "10-20 minutes";
							}
							else if ($row["videolength"] <= 40) {
								$str = "20-40 minutes";
							}
							else if ($row["videolength"] <= 60) {
								$str = "40-60 minutes";
							}
							else {
								$str = "Over 60 minutes";
							}
			
							echo "<td>$str</td>";
							echo "<td>".$row["highestresolution"]."</td>";
							echo "<td>".$row["language"]."</td>";

							if ($row["viewcount"] <= 75000) {
								$str = "50-70k"; 
							}
							else if ($row["viewcount"] <= 100000) {
								$str = "75-100k";
							}
							else if ($row["viewcount"] <= 125000) {
								$str = "100-125k";
							}
							else if ($row["viewcount"] <= 150000) {
								$str = "125-150k";
							}
							else {
								$str = "Over 150k";
							}

							echo "<td>$str</td>";
							echo "<td>".$row["tag"]."</td>";

							if ($loggedin && $username === "admin@admin.com") {
								echo "<td>
										<form method=\"post\" action=\"update_entry.php\">
											<input type=\"hidden\" name=\"video\" value=\"{$row['title']}\">
											<input type=\"hidden\" name=\"link\" value=\"{$row['videolink']}\">
											<input type=\"hidden\" name=\"length\" value=\"{$row['videolength']}\">
											<input type=\"hidden\" name=\"quality\" value=\"{$row['highestresolution']}\">
											<input type=\"hidden\" name=\"desc\" value=\"{$row['description']}\">
											<input type=\"hidden\" name=\"lang\" value=\"{$row['language']}\">
											<input type=\"hidden\" name=\"views\" value=\"{$row['viewcount']}\">
											<input type=\"hidden\" name=\"cat\" value=\"{$row['videotype']}\">
											<input type=\"hidden\" name=\"thumbnail\" value=\"{$row['iconimage']}\">
											<input type=\"hidden\" name=\"tags\" value=\"{$row['tag']}\">
											<input type=\"hidden\" name=\"offset\" value=\"$offset\">
											<input type=\"hidden\" name=\"id\" value={$row['id']}>
											<input type=\"hidden\" name=\"referrer\" value=\"search.php\">
											<input type=\"hidden\" name=\"search\" value=\"$searchterms\">
											<input type=\"hidden\" name=\"type\" value=\"$type\">
											<input type=\"hidden\" name=\"martialarts\" value=\"$martialarts\">
											<input type=\"submit\" value=\"edit\">	
										</form>
									</td>
									";
							}
							
							if ($loggedin)
							{
								if ( count($favorites) > 0 )
								{
									$found = false;
									foreach ($favorites as $key => $value)
									{
										if ($row['id'] === $value)
										{
											$found = true;
											echo "<td>
													<form method=\"post\" action=\"search.php?page=$offset&search=$searchterms&type=$type\">
														<input type=\"hidden\" name=\"remove_favorite\" value=\"{$row['id']}\"> 
														<input type=\"submit\" value=\"remove from favorites\">
													</form>
												</td>";
												break;
										}
									}

									if (!$found) {
										echo "<td>
												<form method=\"post\" action=\"search.php?page=$offset&search=$searchterms&type=$type\">
													<input type=\"hidden\" name=\"add_favorite\" value=\"{$row['id']}\"> 
													<input type=\"submit\" value=\"add to favorites\">
												</form>
										</td>";
									}
									
								}
								else
								{
									echo "<td>
											<form method=\"post\" action=\"search.php?page=$offset&search=$searchterms&type=$type\">
												<input type=\"hidden\" name=\"add_favorite\" value=\"{$row['id']}\"> 
												<input type=\"submit\" value=\"add to favorites\">
											</form>
										</td>";
								}
							}
							echo "<td>".$row["category"]."</td>";
							echo "</tr>";
							$flagged = $flagged + 1;
						}
					}
									
					echo "</tbody></table>";
				}
				else {
					echo "</tbody></table>";
					echo "<h1>There are no videos!</h1>";
					var_dump($searchterms);
					$flagged = 0;
				}
				if($flagged != 0) {
					$previous = $offset-10;
					if ($offset != 0)
						echo "<a href=\"search.php?page=$previous&search=$searchterms&type=$type&res=$res&views=$views&lang=$lang&martialarts=$martialarts\"><< PREVIOUS</a>&nbsp;";
					$offset += 10;
					if ($flagged == 10)
						echo "<a href=\"search.php?page=$offset&search=$searchterms&type=$type&res=$res&views=$views&lang=$lang&martialarts=$martialarts\">NEXT >></a>";
				}
			}
			
		?>
		
	</div>
	<div id="footer">
		<div>
			<div class="connect">
				<a href="http://twitter.com/#!/sjsu" id="twitter">twitter</a>
				<a href="http://www.facebook.com/sanjosestate" id="facebook">facebook</a>
				<a href="http://pinterest.com/sjsu/" id="pinterest">pinterest</a>
			</div>
			<p>
				&copy; copyright 2014 | all rights reserved.
			</p>
		</div>
	</div>
</body>
</html>