<?php
	session_start();
?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Team 5 | Video Submission</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div id="header">
		<div class="section">
			<div class="logo">
				<a href="index.php">Shaolin</a>
			</div>
			<nav>
				<ul>
					<li>
						<a href="submission.php">SUBMIT</a>
					</li>
					<li>
						<a href="#">CATEGORY</a>
							<ul>
							<li><a href="category.php?cat=Tutorial">Tutorials</a></li>
							<li><a href="category.php?cat=Entertainment">Entertainment</a></li>
							<li><a href="category.php?cat=Application">Application</a></li>
							<li><a href="category.php?cat=Weapon">Weapons</a></li>
							<li><a href="category.php?cat=Group demo">Group demos</a></li>
							<li><a href="category.php?cat=Others">Others</a></li>
							</ul>
					</li>
					<li>
						<a href="#">videos</a>
							<ul>
							<li><a href="list.php">List videos</a></li>
							<li><a href="search.php">Search videos</a></li>
							<li><a href="update_entry.php">Update videos</a></li>
							</ul>
					</li>
					<li class="selected">
						<a href="#">Profile</a>
							<ul>
							<li><a href="register.php">Register</a></li>
							<?php
								echo "<li><a href=\"favorites.php\">Favorites</a></li>";
								if (isset($_SESSION['loggedIn'])) {
									echo "<li><a href=\"logout.php\">Logout</a></li>";
								}
								else
									echo "<li><a href=\"login.php\">Login</a></li>";
							?>
							</ul>
					</li>
					<li>
						<a href="about.php">about</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div id="body">
		<div class="submission">
		<h3>Submit a New Martial Arts Video</h3>
				
		<form method=post id="daform" action="submit.php">
		<b>Title:</b>
		<input type=text size=80 name=video required><br/>
		<br>
		<b>Link:</b>
		<input type=text size=40 name=link required><br/>
		<br>
		<b>Video Length:</b>
		<select name=length>
			<option value=5>0-5m</option>
			<option value=20>10-20m</option>
			<option value=40>20-40m</option>
			<option value=60>40-60m</option>
			<option value=99>60m+</option>
		</select><br><br>
		<b>Video Quality:</b>
		<select name="quality">
			<option value="144">144p</option>
			<option value="240">240p</option>
			<option value="360">360p</option>
			<option value="480">480p</option>
			<option value="720">720p</option>
			<option value="1080">1080p</option>
			<option value="9999">1440p/2K/4K</option>
		</select><br>
		<br>
		<b>Description:</b></br>
		<textarea name="desc" form="daform" rows="3" wrap="virtual" cols="30" maxlength="1000" placeholder="up to 1000 characters"></textarea>
		<br>
		<b>Language:</b>
		<input type="radio" name="lang" value="English"><label>English</label>
		<input type="radio" name="lang" value="Non-English"><label>Non-English</label><br>
		<br>
		<b>View Count:</b>
		<select name="views">
			<option value=75000>50k-70k</option>
			<option value=100000>75k-100k</option>
			<option value=125000>100k-125k</option>
			<option value=150000>125k-150k</option>
			<option value=999999>150k+</option>
		</select><br><br>
		<b>Video Type:</b>
		<input type="checkbox" name="cat[]" value="Tutorial"><label>Tutorial</label>
		<input type="checkbox" name="cat[]" value="Entertainment"><label>Entertainment</label>
		<input type="checkbox" name="cat[]" value="Application"><label>Application</label><br>
		<input type="checkbox" name="cat[]" value="Weapon"><label>Weapon</label>
		<input type="checkbox" name="cat[]" value="Group demo"><label>Group demo</label>
		<input type="checkbox" name="cat[]" value="Others"><label>Others</label><br>
		<br>
		<b>Thumbnail:</b>
		<input type=text size=40 name=thumbnail><br/>
		<br><br><b>Tags</b> (seperate with commas)</br>
		<textarea name="tags" form="daform" rows="3" maxRows="3" wrap="virtual" cols="30" maxlength="100" placeholder="cool,amazing,awesome"></textarea>
		<br>
		<select name ="martialarts">
			<option value="">Martial arts</option>
			<option value="Yang Yaichi">Yang Taichi</option>
			<option value="Chen Taichi">Chen Taichi</option>
			<option value="Sun Taichi">Sun Taichi</option>
			<option value="Wu Taichi">Wu Taichi</option>
			<option value="QiGong">QiGong</option>
			<option value="Shaolin">Shaolin</option>
			<option value="Tae kwon do">Tae kwon do</option>
			<option value="Wing Chun">Wing Chun</option>
			<option value="Aikido">Aikido</option>
			<option value="Judo">Judo</option>
			<option value="KungFu Movie">KungFu Movie</option>
		</select>
		<br><br>
		<input type=submit name=submit value="Submit">
		<input type=reset name=reset value="Reset">

		</form>
		</div>
	</div>
	<div id="footer">
		<div>
			<div class="connect">
				<a href="http://twitter.com/#!/sjsu" id="twitter">twitter</a>
				<a href="http://www.facebook.com/sanjosestate" id="facebook">facebook</a>
				<a href="http://pinterest.com/sjsu/" id="pinterest">pinterest</a>
			</div>
			<p>
				&copy; copyright 2014 | all rights reserved.
			</p>
		</div>
	</div>
</body>
</html>